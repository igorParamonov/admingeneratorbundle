<?php

namespace Admingenerator\GeneratorBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;

class ColorsCompilerPass implements CompilerPassInterface
{
    /**
     * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
     * @return \Symfony\Bundle\AsseticBundle\FilterManager
     */
    protected function getFilterManager(ContainerBuilder $container)
    {
        return $container->get('assetic.filter_manager');
    }

    /**
     * @param \Symfony\Bundle\AsseticBundle\FilterManager $asseticFilterManager
     * @return \Assetic\Filter\LessphpFilter
     */
    protected function getLessphpFilter($asseticFilterManager)
    {
        return $asseticFilterManager->get('lessphp');
    }

    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasParameter('assetic.filter.lessphp.presets')) {
            throw new InvalidConfigurationException('You must identify the "lessphp" filter');
        }

        $asseticFilterLessphpPresets    = $container->getParameter('assetic.filter.lessphp.presets');
        $admingeneratorColors           = $container->getParameter('admingenerator.colors');
        
        foreach ($admingeneratorColors as $key => $value) {
            $asseticFilterLessphpPresets["color-" . str_replace('_', '-', $key)] = $value;
        }
        
        $container->setParameter('assetic.filter.lessphp.presets', $asseticFilterLessphpPresets);
    }
}