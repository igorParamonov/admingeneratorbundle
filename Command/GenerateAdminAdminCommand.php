<?php

namespace Admingenerator\GeneratorBundle\Command;

use Admingenerator\GeneratorBundle\Routing\Manipulator\RoutingManipulator;
use Admingenerator\GeneratorBundle\Generator\BundleGenerator;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Sensio\Bundle\GeneratorBundle\Command\GenerateBundleCommand;
use Sensio\Bundle\GeneratorBundle\Command\Validators;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class GenerateAdminAdminCommand extends GenerateBundleCommand
{
    /** @var InputInterface */
    protected $input;

    /** @var OutputInterface */
    protected $output;

    protected function configure()
    {
        $this
            ->setName('admin:generate-admin')
            ->setDescription('Generate admin classes into an existant bundle')
            ->setDefinition(array(
                new InputOption('namespace', '', InputOption::VALUE_REQUIRED, 'The namespace of the bundle to create'),
                new InputOption('dir', '', InputOption::VALUE_REQUIRED, 'The directory where to create the bundle'),
                new InputOption('bundle-name', '', InputOption::VALUE_REQUIRED, 'The optional bundle name'),
                new InputOption('structure', '', InputOption::VALUE_NONE, 'Whether to generate the whole directory structure'),
                new InputOption('format', '', InputOption::VALUE_REQUIRED, 'Do nothing but mandatory for extend', 'annotation'),
                new InputOption('generator', '', InputOption::VALUE_REQUIRED, 'The generator service (propel, doctrine, doctrine_odm)', 'doctrine'),
                new InputOption('model-name', '', InputOption::VALUE_REQUIRED, 'Base model name for admin module, without namespace.', 'YourModel'),
                new InputOption('prefix', '', InputOption::VALUE_REQUIRED, 'The generator prefix ([prefix]-generator.yml)'),
                new InputOption('export', '', InputOption::VALUE_REQUIRED, 'The generator button for export?'),

            ))
            ->setHelp(<<<EOT
The <info>admin:generate-admin</info> command helps you generates new admin controllers into an existant bundle.
EOT
            )
        ;
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->input  = $input;
        $this->output = $output;

        $output->writeln('<comment>Create controllers for a generator module</comment>');

        $input->setOption('generator', $input->getOption('generator') ?: 'doctrine');

        // bundle name
        $bundle = $input->getOption('bundle-name')
            ?: strtr($input->getOption('namespace'), array('\\Bundle\\' => '', '\\' => ''));
        $output->writeln(array(
            '',
            'In your code, a bundle is often referenced by its name. It can be the',
            'concatenation of all namespace parts but it\'s really up to you to come',
            'up with a unique name (a good practice is to start with the vendor name).',
            'Based on the namespace, we suggest <comment>' . $bundle . '</comment>.',
            '',
        ));
        $input->setOption('bundle-name', $bundle);

        // target dir
        $dir = $input->getOption('dir') ?: dirname($this->getContainer()->getParameter('kernel.root_dir')) . '/src';
        $output->writeln(array(
            '',
            'The bundle can be generated anywhere. The suggested default directory uses',
            'the standard conventions.',
            '',
        ));
        $input->setOption('dir', $dir);

        // prefix
        $input->setOption('prefix', $input->getOption('prefix') ?: $input->getOption('model-name'));

        // export

        $export = $input->getOption('export') ?: 'yes';

        if ($export) {
            $output->writeln('Button Generated');
        }
        $input->setOption('export', $export);
    }

     /**
     * @see Command
     *
     * @throws \InvalidArgumentException When namespace doesn't end with Bundle
     * @throws \RuntimeException         When bundle can't be executed
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach (array('namespace', 'dir') as $option) {
            if (null === $input->getOption($option)) {
                throw new \RuntimeException(sprintf('The "%s" option must be provided.', $option));
            }
        }

        $namespace = Validators::validateBundleNamespace($input->getOption('namespace'));
        if (!$bundleName = $input->getOption('bundle-name')) {
            $bundleName = strtr($namespace, array('\\' => ''));
        }
        $bundleName = Validators::validateBundleName($bundleName);
        $format = Validators::validateFormat($input->getOption('format'));
        $dir = $input->getOption('dir') . '/';
        $structure = $input->getOption('structure');

        if (!$this->getContainer()->get('filesystem')->isAbsolutePath($dir)) {
            $dir = getcwd().'/'.$dir;
        }

        $generatorName = $input->getOption('generator');
        $modelName = $input->getOption('model-name');

        $generator = $this->createGenerator();
        $generator->setGenerator($generatorName);
        $generator->setPrefix($input->getOption('prefix'));
        $generator->setExport(($input->getOption('export')) ? '~' : '');
        $generator->generate($namespace, $bundleName, $dir, $format, $structure, $generatorName, $modelName);

        $output->writeln('Generating the bundle code: <info>OK</info>');

        $bundle = $this->getContainer()->get('kernel')->getBundle($bundleName);

        $this->updateRoutes($output, $bundle);
    }

    protected function createGenerator()
    {
        return new BundleGenerator($this->getContainer()->get('filesystem'), __DIR__.'/../Resources/skeleton/bundle');
    }

    protected function updateRoutes(OutputInterface $output, Bundle $bundle)
    {
        $auto = true;

        $output->write('Importing the bundle routing resource: ');
        $targetRoutingPath = $this->getContainer()->getParameter('kernel.root_dir').'/config/routing.yml';
        $output->write(sprintf(
            '> Importing the bundle\'s routes from the <info>%s</info> file: ',
            $this->makePathRelative($targetRoutingPath)
        ));
        $routing = new RoutingManipulator($targetRoutingPath);

        try {
            $ret = $auto ? $routing->addResource($bundle->getName(), 'admingenerator') : false;
            if (!$ret) {
                $help = sprintf("        <comment>resource: \"@%s/Controller/%s/\"</comment>\n" .
                    "        <comment>type:     admingenerator</comment>\n" .
                    "        <comment>prefix:   %s/</comment>\n",
                    $bundle->getName(),
                    ucfirst($this->input->getOption('prefix'))
                );

                return array(
                    '- Import the bundle\'s routing resource in the app main routing file:',
                    '',
                    sprintf('    <comment>%s:</comment>', $bundle->getName()),
                    $help,
                    '',
                );
            }
        } catch (\RuntimeException $e) {
            return array(
                sprintf('Bundle <comment>%s</comment> is already imported.', $bundle->getName()),
                '',
            );
        }
    }
}