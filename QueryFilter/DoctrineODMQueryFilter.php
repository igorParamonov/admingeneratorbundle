<?php

namespace Admingenerator\GeneratorBundle\QueryFilter;

class DoctrineODMQueryFilter extends BaseQueryFilter
{
    /**
     * @param string $field
     * @param string $value
     */
    public function addDefaultFilter($field, $value)
    {
        $this->query->field($field)->equals($value);
    }

    /**
     * @param string $field
     * @param string $value
     */
    public function addStringFilter($field, $value)
    {
        $regex = new \MongoRegex('/.*' . preg_quote($value) . '.*/i');
        $this->query->field($field)->equals($regex);
    }

    /**
     * @param string $field
     * @param boolean $value
     */
    public function addBooleanFilter($field, $value)
    {
        if ("" !== $value) {
            $this->query->field($field)->equals((boolean)$value);
        }
    }

    /**
     * @param string $field
     * @param array  $value
     * @param string $format
     */
    public function addDateFilter($field, $value, $format = 'Y-m-d H:i:s')
    {
        if (is_array($value)) {
            $from = array_key_exists('from', $value) ? $this->formatDate($value['from'], $format) : false;
            $to   = false;
            if (array_key_exists('to', $value)) {
                $value['to']->setTime(23, 59, 59);
                $to = $this->formatDate($value['to'], $format);
            }

            if ($to && $from) {
                $this->query->field($field)->range($from, $to);
            } elseif ($from) {
                $this->query->field($field)->gte($from);
            } elseif ($to) {
                $this->query->field($field)->lte($to);
            }
        } elseif (false !== $date = $this->formatDate($value, $format)) {
            $this->query->field($field)->equals($date);
        }
    }

    /**
     * @param string $field
     * @param object $value
     */
    public function addDocumentFilter($field, $value)
    {
        $this->query->field($field . '.$id')->equals(new \MongoId($value->getId()));
    }

    /**
     * @param string $field
     * @param object $value
     */
    public function addCollectionFilter($field, $value)
    {
        $this->query->field($field . '.$id')->equals(new \MongoId($value->getId()));
    }

    /**
     * @param \DateTime|string $date
     * @param string           $format
     * @return boolean|\MongoDate
     */
    protected function formatDate($date, $format)
    {
        $dateTime = new \DateTime(parent::formatDate($date, $format));
        return $date
            ? new \MongoDate($dateTime->getTimestamp(), $dateTime->format('u'))
            : false;
    }
}