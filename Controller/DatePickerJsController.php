<?php

namespace Admingenerator\GeneratorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class DatePickerJsController extends Controller
{
    /**
     * @Route("/i18n-date-picker-{locale}", name="jquery_ui_date_picker")
     * @Template("AdmingeneratorGeneratorBundle:Common:i18nDatePicker.js.twig")
     *
     * @param string $locale
     *
     * @return array Template context
     */
    public function jqueryUiDatePickerAction($locale)
    {
        $months      = array();
        $shortMonths = array();
        $weeks       = array();
        $shortWeeks  = array();
        for ($i = 1; $i <= 12; $i ++) {
            $dateTime      = new \DateTime('01.' . ($i < 10 ? '0' : '') . $i . '.' . date('Y'));
            $months[]      = datefmt_format_object($dateTime, 'MMMM', $locale);
            $shortMonths[] = datefmt_format_object($dateTime, 'MMM', $locale);
        }
        $sunday = new \DateTime('first sunday of this month');
        for ($i = 0; $i < 7; $i ++) {
            if ($i > 0) {
                date_add($sunday, date_interval_create_from_date_string('1 day'));
            }

            $weeks[]      = datefmt_format_object($sunday, 'eeee', $locale);
            $shortWeeks[] = datefmt_format_object($sunday, 'eee', $locale);
        }

        return array(
            'locale'      => $locale,
            'months'      => $months,
            'shortMonths' => $shortMonths,
            'weeks'       => $weeks,
            'shortWeeks'  => $shortWeeks,
        );
    }
}