<?php

namespace Admingenerator\GeneratorBundle\EventListener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Admingenerator\GeneratorBundle\Exception\NotAdminGeneratedException;

class ControllerListener
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @var \Admingenerator\GeneratorBundle\Parser\ParserIntarface
     */
    protected $parser;

    /**
     * @var array Enviropments to regenerate cache files
     */
    protected $envs;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     * @param array $allowedEnvironmentsToRegenerate
     */
    public function __construct(ContainerInterface $container, array $allowedEnvironmentsToRegenerate = array())
    {
        $this->container    = $container;
        $this->envs         = $allowedEnvironmentsToRegenerate;
        $this->parser       = $container->get('admingenerator.parser.yaml');
    }

    /**
     * On kernel request event handler
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $env = $this->container->get('kernel')->getEnvironment();

        if (in_array($env, $this->envs) && (HttpKernelInterface::MASTER_REQUEST === $event->getRequestType())) {
            try {
                $controller = $event->getRequest()->attributes->get('_controller');

                if (strstr($controller, '::')) { // Check if its a "real controller" not assetic for example
                    $generatorYaml = $this->getGeneratorYml($controller);

                    $generator = $this->getGenerator($generatorYaml);
                    $generator->setGeneratorYml($generatorYaml);
                    $generator->setBaseGeneratorName($this->getBaseGeneratorName($controller));
                    $generator->build();
                }
            } catch (NotAdminGeneratedException $e) {
                // Lets the word running this is not an admin generated module
            }
        }

        if ($this->container->hasParameter('admingenerator.twig')) {
            $twig_params = $this->container->getParameter('admingenerator.twig');

            if (isset($twig_params['date_format'])) {
                $this->container->get('twig')->getExtension('core')->setDateFormat($twig_params['date_format'], '%d days');
            }

            if (isset($twig_params['number_format'])) {
                $this->container->get('twig')->getExtension('core')->setNumberFormat($twig_params['number_format']['decimal'], $twig_params['number_format']['decimal_point'], $twig_params['number_format']['thousand_separator']);
            }
        }
    }

    /**
     * @param string $generatorYaml
     * @return \Admingenerator\GeneratorBundle\Generator\GeneratorInterface
     */
    protected function getGenerator($generatorYaml)
    {
        $yaml = $this->parser->parse($generatorYaml);
        $generator = $this->container->get($yaml['generator']);
        $generator->setParsedData($yaml);

        return $generator;
    }

    /**
     * @param string $controller
     * @return string
     */
    protected function getBaseGeneratorName($controller)
    {
        preg_match('/(.+)Controller(.+)::.+/', $controller, $matches);

        //Find if its a name-generator or generator.yml
        if (isset($matches[2]) && strstr($matches[2], '\\')) {
            if (3 != count(explode('\\', $matches[2]))) {
                return '';
            }

            list(, $generatorName) = explode('\\', $matches[2], 3);

            return $generatorName;
        }

        return '';
    }

    /**
     * @todo Find objects in vendor dir
     */
    protected function getGeneratorYml($controller)
    {
        $matches = array();
        preg_match('/(.+)?Controller.+::.+/', $controller, $matches);
        $dir = str_replace('\\', DIRECTORY_SEPARATOR, $matches[1]);

        $generatorName  = $this->getBaseGeneratorName($controller) ? $this->getBaseGeneratorName($controller).'-' : '';
        $generatorName .= 'generator.yml';

        $finder = new Finder();
        $finder->files()
               ->name($generatorName);

        $rootDir = $this->container->getParameter('kernel.root_dir');
        if (is_dir($src = realpath("$rootDir/../src/$dir/Resources/config"))) {
            $namespaceDirectory = $src;
        } else {
            // glue directory for get namespace
            $bundleName = str_replace(DIRECTORY_SEPARATOR, '', $dir);
            // if bundle exists
            if (key_exists($bundleName, $this->container->get('kernel')->getBundles())) {
                // get bundle by name
                $bundle = $this->container->get('kernel')->getBundle($bundleName);
                // and create namespace from bundle path
                $namespaceDirectory = "{$bundle->getPath()}/Resources/config";
            } else {
                $namespaceDirectory = realpath("$rootDir/../vendor/bundles/$dir/Resources/config");
            }
        }

        if (is_dir($namespaceDirectory)) {
            $finder->in($namespaceDirectory);
            $it = $finder->getIterator();
            $it->rewind();

            if ($it->valid()) {
                return $it->current()->getRealpath();
            }
        }

        throw new NotAdminGeneratedException;
    }
}