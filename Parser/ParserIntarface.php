<?php

namespace Admingenerator\GeneratorBundle\Parser;

interface ParserIntarface
{
    /**
     * Parse generator.yml
     * @param string $file Path to yml file
     * @return array Parsed data
     */
    public function parse($file);
}