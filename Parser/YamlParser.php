<?php

namespace Admingenerator\GeneratorBundle\Parser;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

class YamlParser implements ParserIntarface
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @var \Symfony\Component\HttpKernel\KernelInterface
     */
    protected $kernel;

    /**
     * Constructor
     *
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->kernel    = $container->get('kernel');
    }

    /**
     * {@inheritdoc}
     */
    public function parse($file)
    {
        $data = Yaml::parse($file);
        $parent = $this->checkParentGenerator($file);
        if ($parent) {
            $data = $this->mergeGenerators(Yaml::parse($parent), $data);
        }

        if ($model = $this->parametricModel($data)) {
            $data['params']['model'] = $model;
        }

        if ($inheritableGenerator = $this->extendsGenerator($data)) {
            $data = $this->mergeGenerators(Yaml::parse($inheritableGenerator), $data);
        }

        return $data;
    }

    /**
     * Get model from container parameters if specified as a parameter
     * @param array $data Parsed data
     * @return string|boolean <b>string</b> - Model namespace from parameters<br>
     * <b>boolean</b> - false - If model not specified as parameter
     */
    protected function parametricModel($data)
    {
        // если модель содержит "%"
        return (isset($data['params']['model']) && strstr($data['params']['model'], '%'))
            // вернуть модель из параметров
            ? $this->container->getParameter(trim($data['params']['model'], '%'))
            // иначе вернуть false
            : false;
    }

    /**
     * Check exists generator.yml file in parent bundle
     * @param string $file
     * @return string|null Path to generator.yml file in parent bundle
     */
    protected function checkParentGenerator($file)
    {
        $matches = array();
        preg_match('/.*\\' . DIRECTORY_SEPARATOR . '(.*\\' . DIRECTORY_SEPARATOR . '.*Bundle).*/', $file, $matches);

        list (, $bundle)  = preg_replace('/\\' . DIRECTORY_SEPARATOR . '/', '', $matches);

        $Bundles          = $this->kernel->getBundle($bundle, false);
        $Bundle           = end($Bundles);
        $parentBundleName = $Bundle->getParent();

        if ($parentBundleName !== null) {
            $parentBundles       = $this->kernel->getBundle($parentBundleName, false);
            $parentBundle        = end($parentBundles);
            $parentGeneratorPath = preg_replace('/.*Bundle/', $parentBundle->getPath(), $file);

            if (file_exists($parentGeneratorPath)) {
                return $parentGeneratorPath;
            }
        }
    }

    /**
     * Merge parsed data from parent in children generator.yml-s
     * @param array $parent
     * @param array $children
     * @return array
     */
    protected function mergeGenerators($parent, $children)
    {
        foreach ($children as $key => $value) {
            $parentLink = &$parent;
            foreach (explode('.', $key) as $i) {
                $j = trim($i, '\'');
                if (!key_exists($j, $parentLink)) {
                    $parentLink[$j] = array();
                }
                $parentLink = &$parentLink[$j];
            }
            $parentLink = $value;
        }

        return $parent;
    }

    /**
     * Get inheritable generator if is defined
     * @param array $data
     * @return null|string <b>null</b> If generator dont contains extends<br>
     * <b>string</b> Path to inheritable generator
     */
    protected function extendsGenerator(array $data)
    {
        if (!array_key_exists('extends', $data)) {
            return;
        }

        list ($bundle, $generator) = explode (':', $data['extends']);

        $bundles = $this->kernel->getBundle($bundle, false);

        return end($bundles)->getPath() .
            DIRECTORY_SEPARATOR .
            'Resources' .
            DIRECTORY_SEPARATOR .
            'config' .
            DIRECTORY_SEPARATOR .
            $generator;
    }
}