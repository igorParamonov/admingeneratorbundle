$(document).ready(function () {
    $('body').append(
        $('<div></div>')
            .prop('id', 'colorbox-preloader')
            .addClass('valign-wrapper')
            .css('display', 'none')
            .css('position', 'fixed')
            .css('top', 0)
            .css('left', 0)
            .css('width', '100%')
            .css('height', '100%')
            .css('background-color', 'rgba(255, 255, 255, 0.9)')
            .css('z-index', '1000000')
            .append(
                $('<div></div>')
                    .addClass('preloader-wrapper big active')
                    .css('display', 'block')
                    .css('margin', '0 auto')
                    .append(
                        $('<div></div>')
                            .addClass('spinner-layer spinner-blue-only')
                            .css('border-color', '#fb8c00')
                            .append(
                                $('<div></div>').append($('<div></div>').addClass('circle')).addClass('circle-clipper left'),
                                $('<div></div>').append($('<div></div>').addClass('circle')).addClass('gap-patch'),
                                $('<div></div>').append($('<div></div>').addClass('circle')).addClass('circle-clipper right')
                            )
                    )
            )
    );

    var $colorboxPreloader = $('#colorbox-preloader');

    $.colorbox.settings.trapFocus = false;
    $.colorbox.settings.className = 'card-panel';
    $.colorbox.settings.close = '<i class="mdi-content-clear"></i>';
    $.colorbox.settings.onOpen = function() {
        $colorboxPreloader.fadeIn(0);
    };
    $.colorbox.settings.onComplete = function() {
        $colorboxPreloader.fadeOut(0);
    };

    $.fn.select2.defaults.containerCssClass = 'dropdown-button waves-effect waves-light';

    $('.colorbox').colorbox();
});