$(document).ready(function () {
    
    // Colorbox link
    $('.colorbox').colorbox();
    
    // Popup box
    $('.popup').each(function(){
        $(this).html('<div class="popup_content">' + $(this).html() + '</div>');
        $(this).prepend('<div class="popup_arrow"></div>');
        $(this).addClass('popup_ready');
    });
    $('.popup .popup_arrow').click(function(){
        $(this).parent('div.popup').toggleClass('popup_active');
        $('.popup').not($(this).parent('div.popup')).removeClass('popup_active');
    });
    $('.popup .popup_arrow').hover(function(){
        var height_scroll = document.documentElement.scrollTop;
        var dist_item_to_top = $(this).offset().top - height_scroll;
        var popup = $(this).parent('.popup');
        var height_window = $(window).height(); 
        var height_content = $(popup).children('.popup_content').outerHeight();
        $(popup).toggleClass('above', height_window - dist_item_to_top < height_content + 10);
    });
    
    //Toggle
    $('.toggle').each(function(){
        $('<div class="toggle_arrow"></div>').insertBefore(this);
    });
    $('.toggle_arrow').click(function(){
       $(this).siblings('.toggle').toggle(200);
    });
    
    //Responsive table
    $('.table_responsive').footable({
        breakpoints: {
            phone: 480,
            tablet: 1024,
            desktop_big: 1280
        }
    });

    // select to select2 (переехало в js формы для select-а)
    // $('select').select2({"placeholder":"","width":"element","allowClear":true});
    
    //double_list (blocking one of buttons '<'  or '>')
    $('.double_list_select-unselected').mousedown( 
        function(){  
            $('.to_unselected').addClass('hide'); 
            $('.to_selected').removeClass('hide'); 
        } 
    );   
    $('.double_list_select-selected').mousedown( 
        function(){  
            $('.to_selected').addClass('hide'); 
            $('.to_unselected').removeClass('hide'); 
        } 
    );  
    
    //checkbox/radiobutton
    $('input:checkbox, input:radio').not('.checkbox').after('<i class="check"></i>');
    $('body').delegate('input + i.check', 'click', function(){ 
        $(this).parent().find('input').click();
    });
    
    // Tabs
    $.tabs('.tabs a');
    
    //styling tab when it has error
    $('[name = "save"] , [name = "save-and-list"]').on('click', function(e){
        $('.tabs a').each(function(i){
            $(this).toggleClass('has_error', ($($(this).data('tab')).find('.errors')).length > 0);
        });
    });
});
   

// Общие функции
function common() {
    this.flash = flash;
    
    // Вывод флеш-сообщения
    function flash (message, type) {
       
        if(!message) return;
        
        type = type || 'warning';
        
        $('.notification_box').remove();
        
        $('.content').before('<div class="notification_box ' + type + '">' + message + '</div>');
   }
}
(function(){
    common = new common();
})();