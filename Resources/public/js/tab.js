$.tabs = function(e, t) {
    $(e).each(function(t, n) {
        $($(n).data("tab")).css("display", "none");
        $($(n).data("tab")).css("overflow", "hidden");
        $($(n).data("tab")).addClass('tab_content');
        $(n).click(function() {
            $(e).each(function(e, t) {
                $(t).removeClass("selected");
                $($(t).data("tab")).css("display", "none");
                $($(t).data("tab")).css("overflow", "hidden")
            });
            $(this).addClass("selected");
            $($(this).data("tab")).css("display", "block");
            $($(this).data("tab")).css("overflow", "visible")
        })
    });
    if (!t) {
        t = $(e + ":first").data("tab")
    }
    $(e + "[data-tab='" + t + "']").trigger("click")
}