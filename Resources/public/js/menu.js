$(document).ready(function() {
    flag = true; // флаг для того чтоб повторно не делались изменения в моб версии
    $('.menu_drop > ul > li').click(function() {
        // Для мобильной версии
        if ($(this).parents('div.menu_drop').hasClass('visible')) {
            if (flag) {
                $(this).toggleClass('active').siblings('li.active').removeClass('active');
            } else {
                flag = true;
            }
        }
        // Для полной версии
        else {
            $(this).addClass('active').siblings('li.active').removeClass('active');
            // add class if menu has submenu
            if ($(this).children('ul').length > 0) {
                $(this).parents('div.menu_drop').addClass('has_submenu');
            } else {
                $(this).parents('div.menu_drop').removeClass('has_submenu');
            }

//            $(this).parents('div.menu_drop').removeClass('has_submenu_level2');
        }
    })
    $('.menu_drop > ul > li > ul > li').click(function() {
        // Для мобильной версии
        if ($(this).parents('div.menu_drop').hasClass('visible')) {
            flag = false;
            $(this).parents('div.menu_drop').find('li.current').removeClass('current');
            $(this).toggleClass('active');
//           return false;
        }
        // Для полной версии
        else {
            $(this).addClass('active').siblings('li.active').removeClass('active');
            // add class if menu has submen
            if ($(this).children('ul').length > 0) {
                $(this).parents('div.menu_drop').addClass('has_submenu_level2');
            } else {
                $(this).parents('div.menu_drop').removeClass('has_submenu_level2');
            }
        }
    });

    // first display
    if ($('.menu_main .active').children('ul').length > 0) {
        $('.menu_main').addClass('has_submenu');
    }
    if ($('.menu_main .active ul li.active').children('ul').length > 0) {
        $('.menu_main').addClass('has_submenu_level2');
    }


    //add current item menu to add/edit forms
    var url = document.location.href;
    $('div.menu_main a').each(function(i, item) {
        if (url.match($(item).prop('href'))) {
            $(item).parents('li').addClass('current').click();
            $(item).parent('li').removeClass('active');
        }
    });
});

// Show / hide block
function toggleBlock(block, init) {
    $(block).toggleClass('visible');
    $('.visible').not(block).removeClass('visible');
    $(init).toggleClass('active').siblings('.active').removeClass('active');
    return false;
}