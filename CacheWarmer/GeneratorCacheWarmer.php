<?php

namespace Admingenerator\GeneratorBundle\CacheWarmer;

use Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Admingenerator\GeneratorBundle\Exception\GeneratedModelClassNotFoundException;

/**
 * Generate all admingenerated bundle on warmup
 *
 * @author Cedric LOMBARDOT
 * @author nitra
 */
class GeneratorCacheWarmer implements CacheWarmerInterface
{
    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;

    /** @var \Admingenerator\GeneratorBundle\CacheWarmer\GeneratorFinder */
    protected $finder;

    /** @var \Admingenerator\GeneratorBundle\Parser\ParserIntarface */
    protected $parser;

    /**
     * Constructor.
     *
     * @param ContainerInterface $container The dependency injection container
     */
    public function __construct(ContainerInterface $container, GeneratorFinder $finder)
    {
        $this->container = $container;
        $this->finder    = $finder;
        $this->parser    = $container->get('admingenerator.parser.yaml');
    }

    /**
     * Warms up the cache.
     *
     * @param string $cacheDir The cache directory
     */
    public function warmUp($cacheDir)
    {
        $yamls = $this->finder->findAllGeneratorYamls();
        foreach ($yamls as $yaml) {
            try {
                $this->buildFromYaml($yaml);
            } catch (GeneratedModelClassNotFoundException $e) {
                echo '>> Skip warmup ' . $e->getMessage() . "\n";
            }
        }
    }

    /**
     * Checks whether this warmer is optional or not.
     *
     * @return Boolean always false
     */
    public function isOptional()
    {
        return false;
    }

    /**
     * Build cache files by generator.yml
     *
     * @param string $file Path to yml file
     */
    protected function buildFromYaml($file)
    {
        $data = $this->parser->parse($file);
        $service = $data['generator'];

        $generator = $this->container->get($service);
        $generator->setParsedData($data);
        $generator->setGeneratorYml($file);

        // windows support too
        $matches = array();
        if (preg_match('/(?:\/|\\\\)([^\/\\\\]+?)-generator.yml$/', $file, $matches)) {
            $generator->setBaseGeneratorName(ucfirst($matches[1]));
        } else {
            $generator->setBaseGeneratorName('');
        }

        $generator->build();
    }
}